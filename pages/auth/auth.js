// pages/auth/auth.js
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    handleAuth(){
        wx.getUserProfile({
          desc: '用于完善用户信息',
          success:(res)=>{
            //   console.log(res)
            //   console.log(res.userInfo)
              wx.setStorageSync('token', res.userInfo) //存入用户信息
              wx.navigateTo({
                url: '/pages/telephone/telephone',
              })
          }
        })
    },

    handleBack(){
        wx.navigateBack({
            delta:2,
        })
      },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        wx.showToast({
          title: '请授权登录以获取更多功能！',
          icon:"none",
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})