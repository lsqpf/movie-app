// pages/getmoney/getmoney.js
import request from "../../utils/request"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        agree:'',
        money:'',
        movieData:[],
        movieList:[],
        cinema:'',
        seat:'',
        movieId:'',
        currentDate:'',
        img:null,
        nm:'',
        actionDate:'',
        overDate:'',
    },

    overAction(){
        if (this.data.agree.length===0) {
            request({
                url:'/buymovies',
                method:'post',
                data:{
                    seat:this.data.seat,
                    movieId:this.data.movieList[0].id,
                    currentDate:this.data.currentDate,
                    price:this.data.money,
                    img:this.data.movieList[0].img,
                    nm:this.data.movieList[0].nm,
                    cinema:this.data.cinema,
                    actionDate:this.data.actionDate,
                    overDate:this.data.overDate,
                    tel:wx.getStorageSync('tel'),
                    nickName:wx.getStorageSync('token').nickName
                }
            })
            wx.showToast({
                title: '正在付款...',
                icon:'loading'
              })
            setTimeout(() => {
                wx.showToast({
                  title: '付款成功！请及时观影~',
                  icon:'none'
                })
            }, 2000);
            setTimeout(() => {
                wx.reLaunch({
                    url: `/pages/home/home?pageItem=1`,
                  })
            }, 4000);
        }else{
            wx.showToast({
              title: '请勿重复购票！',
              icon:'error'
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // console.log(options)
        this.setData({
            money:options.total,
            agree:options.res,
            cinema:options.cinema,
            currentDate:options.currentDate,
            actionDate:options.actionDate,
            overDate:options.overDate,
            seat:options.seat
        })     
        const eventChannel = this.getOpenerEventChannel()
        eventChannel.on('', (data)=>
            // console.log(data)
            this.setData({
                movieList:data
            })
          )   
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        var arr  = Object.values(this.data.movieList)[0]  //从对象中取出第一个数据
        // console.log(arr)
        this.setData({
            movieList:arr
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})