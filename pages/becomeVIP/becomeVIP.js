// pages/becomeVIP/becomeVIP.js
import request from "../../utils/request"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        vip:"1"
    },

    outPage(){
        wx.switchTab({
          url: '/pages/personal/personal',
        })
    },

    createVip(){
        wx.showToast({
            title: '加入会员成功！',
          })
          setTimeout(() => {
            wx.setStorageSync('vip', this.data.vip)
            request({
                url:`/users?tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`
            }).then(res=>{
                // console.log(res)
                request({
                    url:`/users/${res[0].id}`,
                    method:'put',
                    data:{
                        nickName:wx.getStorageSync('token').nickName,
                        tel:wx.getStorageSync('tel'),
                        avatarUrl:res[0].avatarUrl,
                        vip:this.data.vip
                    }
                })
            })
          }, 2000);
          setTimeout(() => {
            wx.switchTab({
                url: '/pages/personal/personal',
              })
          }, 2000);
        
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})