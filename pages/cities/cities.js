import request from "../../utils/request"

// pages/cities/cities.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cityList:[],
        CurrentCity:'南宁'
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.rendercities()
    },

    rendercities(){
        request({
            url:"/cities"
        }).then(res=>{
            // console.log(res)
            this.setData({
                cityList:res
            })
        })
    },

    changeCity(e){
        // console.log(e.currentTarget.dataset.cityid)
        var id = e.currentTarget.dataset.cityid
        request({
            url:`/cities?cityid=${id}`
        }).then(res=>{
            // console.log(res[0].name)
            this.setData({
                CurrentCity:res[0].name
            })
            wx.reLaunch({  //关闭所有页面，打开到应用内的某个页面 (可以带参数)
              url: `/pages/home/home?id=${id}`,
            })
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})