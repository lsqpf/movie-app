// pages/video/video.js
import request from "../../utils/request"
import CheckAuth from "../../utils/auth"
Page({

    /**
     * 页面的初始数据
     */
    sjian:null,
    inputValue:"",
    data: {
        VideoList: [],
        danmuList: [{
            text: '不错不错',
            color: '#ff0000',
            time: 1
          }, {
            text: '哈哈哈哈',
            color: '#ff00ff',
            time: 3
          },
          {
            text: '果断关注了',
            color: '#ffffff',
            time: 5
          }
        ],
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.renderVideo()
    },

    renderVideo() {
        request({
            url: "/video"
        }).then(res => {
            this.setData({
                VideoList: res
            })
        })
    },

    add(e) {
        CheckAuth(()=>{
          
          var star = e.currentTarget.dataset.star
          var id = e.currentTarget.dataset.id
          var title = e.currentTarget.dataset.title
          var url = e.currentTarget.dataset.url
          var videoId = e.currentTarget.dataset.videoId
          request({
            url: `/Video/${id}`,
            method: "put",
            data: {
                "star": star + 1,
                "url": url,
                "title": title
            }
        })
        this.renderVideo()
            console.log("准备标星")
            request({
              url:`/star?videoId=${id}&tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`
            }).then(res=>{
              if (res.length===0) {
                request({
                  url:'/star',
                  method:"post",
                  data:{
                    videoId:id,
                    url:url,
                    title:title,
                    tel:wx.getStorageSync('tel'),
                    nickName:wx.getStorageSync('token').nickName
                  }
                })
              }else{
                request({
                  url: `/Video/${id}`,
                  method: "put",
                  data: {
                      "star": star + 1,
                      "url": url,
                      "title": title
                  }
              })
              this.renderVideo()
              }
            })
        })
        
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        console.log("下拉了")
        setTimeout(() => {
            // 更新数据
            this.renderVideo()
            wx.stopPullDownRefresh({ //停止下拉刷新
                success: (res) => {

                },
            })
        }, 1000)
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})