// pages/personal/personal.js
import CheckAuth from '../../utils/auth'
import request from "../../utils/request"

Page({

    /**
     * 页面的初始数据
     */
    data: {
        userInfo:null,
        abc:1,
        Headimage:'/images/临时头像.jpg',
        vip:""
    },

    handleTap(){
        wx.chooseMedia({
            count: 1,
            mediaType: ['image','video'],  //文件类型
            sourceType: ['album', 'camera'],  //图片和视频选择的来源
            maxDuration: 30, //拍摄视频最长拍摄时间，单位秒。时间范围为 3s 至 60s 之间。不限制相册。
            camera: 'back', //仅在 sourceType 为 camera 时生效，使用前置或后置摄像头
            success:(res)=> {
            //   console.log(res.tempFiles[0].tempFilePath)
              var imageRoute = res.tempFiles[0].tempFilePath
              this.setData({
                  userInfo:{
                    ...this.data.userInfo,
                    avatarUrl:imageRoute,
                  }
              })
              this.setData({
                  Headimage:imageRoute
              })
            //   console.log(this.data.userInfo)
              wx.setStorageSync('token', {
                  ...wx.getStorageSync('token'),
                  avatarUrl:imageRoute
              })
            }
          })
    },

    checkAuth(){
        wx.navigateTo({
          url: '/pages/auth/auth',
        })
    },

    handleError(e){
        // console.log(e)
        this.setData({
            abc:2,
        })
        request({
            url:`/comments?tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`,
            method:"put",
            data:{
                ...this.data.userInfo,
                avatarUrl:this.data.Headimage
            }
        })
        this.onShow()
    },

    handleLike(){
        wx.navigateTo({
          url: '/pages/like/like',
        })
    },

    handleBack(){
        wx.clearStorage()
        wx.reLaunch({
          url: '/pages/personal/personal',
        })
    },

    handleWatch(){
        wx.navigateTo({
          url: '/pages/watch/watch',
        })
    },

    handleVideo(){
      wx.navigateTo({
        url: '/pages/star/star',
      })
    },

    handleComments(){
      wx.navigateTo({
        url: '/pages/comments/comments',
      })
    },

    handleAlready(){
        wx.navigateTo({
          url: '/pages/alreadyBuy/alreadyBuy',
        })
    },

    handleVIP(){
        if (wx.getStorageSync('vip').length!==0) {
            wx.showToast({
              title: '您已成为会员！',
              icon:'error'
            })
        }else{
            wx.navigateTo({
                url: '/pages/becomeVIP/becomeVIP',
              })
        }
        
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
            this.setData({
                userInfo:wx.getStorageSync('token'),
                vip:wx.getStorageSync('vip')
            })
            console.log(this.data.userInfo)
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})