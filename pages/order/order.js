// pages/order/order.js
import CheckAuth from "../../utils/auth"
import request from "../../utils/request"
const QR = require('../../utils/wxqrcode'); //引入模块。返回模块通过 module.exports 或 exports 暴露的接口
Page({

    /**
     * 页面的初始数据
     */
    data: {
        movieData:[],
        movieList:[],
        seatings: [0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
            0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
            0,1,1,1,1,2,2,2,2,2,1,1,1,1,0,
            0,1,1,1,1,2,2,2,2,2,1,1,1,1,0,
            0,0,0,1,1,2,2,2,2,2,1,1,0,0,0,
            0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,
            0,0,0,1,1,1,1,1,1,1,1,1,0,0,0],
        money:'',
        total:"",
        cinema:'',
        seat:'',
        qrcode:'',
        show:1
    },

    buyover(){
            request({
                url:`/buymovies?movieId=${this.data.movieList[0].id}&currentDate=${this.data.movieData[0].Date}&cinema=${this.data.cinema}&actionDate=${this.data.movieData[0].time1}&seat=${this.data.seat}&tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`
            }).then(res=>{
                if (res.length===0) {
                    wx.navigateTo({
                        url: `/pages/getmoney/getmoney?res=${res}&total=${this.data.total}&seat=${this.data.seat}&cinema=${this.data.movieData[0].cinema}&currentDate=${this.data.movieData[0].Date}&actionDate=${this.data.movieData[0].time1}&overDate=${this.data.movieData[0].empty1}`,
                        success:(res)=>{
                            // 通过 eventChannel 向被打开页面传送数据
                            res.eventChannel.emit("",{data:this.data.movieList})
                            // console.log(res)
                        }
                      })
                }else{
                    wx.showToast({
                      title: '请勿重复购票！',
                      icon:'error'
                    })
                }
            })
    },

    getQRCodeSize(){
        var size = 0;
        try{
            var res = wx.getSystemInfoSync(); //异步获取系统信息。需要一定的微信客户端版本支持，在不支持的客户端上，会使用同步实现来返回
            var scale = res.windowWidth / 750//不同屏幕下QRcode的适配比例；设计稿是750rpx宽
            var width = 300 * scale
            size = width
        }catch(e){
            console.log('获取设备信息失败'+e)
            size = 150
        }
        return size
    },
    
    createQRCode(size){
        try{
            let img = QR.createQrCodeImg(('电影名：')+('《')+(this.data.movieList[0].nm)+('》，')+('总金额为')+(this.data.total)+('元'),{size:parseInt(size)}) //parseInt解析到整数部分
            this.setData({
                qrcode:img,
                show:2
            })
        }catch(e){
            console.log(e)
        }
    },

    dieShow(){
        this.setData({
            show:1
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        wx.setNavigationBarTitle({
          title: options.cinema,
        })
        // console.log(options)
        this.setData({
            money:Number(options.price),
            movieData:Array(options),
            cinema:options.cinema
        })
        const eventChannel = this.getOpenerEventChannel()
        eventChannel.on('', (data)=>
            this.setData({
                movieList:data
            })
          )
          this.getQRCodeSize()
    },

    // 如果一个页面由另一个页面通过 wx.navigateTo 打开，这两个页面间将建立一条数据通道：被打开的页面可以通过 this.getOpenerEventChannel() 方法来获得一个 EventChannel 对象； wx.navigateTo 的 success 回调中也包含一个 EventChannel 对象。 这两个 EventChannel 对象间可以使用 emit 和 on 方法相互发送、监听事件。

    handleParentEvent(e){
    //   console.log(e.detail)
      if (wx.getStorageSync('vip').length!==0) {
        this.setData({
            total:(this.data.money*e.detail*0.85).toFixed(1)  //toFixed(1)保留两位小数
          })
      }else{
          this.setData({
        total:(this.data.money*e.detail).toFixed(1)  //toFixed(1)保留两位小数
      })
      }
    },

    handleEvent(e){
        // console.log(e.detail)
        this.setData({
            seat:e.detail
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
        var arr  = Object.values(this.data.movieList)[0]  //从对象中取出第一个数据
        // console.log(arr)
        this.setData({
            movieList:arr
        })
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        CheckAuth(()=>{
            console.log("准备选座购票")
            if (wx.getStorageSync('vip').length!==0) {
                wx.showToast({
                  title: '您是尊贵的VIP会员,购票可享8.5折优惠！',
                  icon:'none'
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})