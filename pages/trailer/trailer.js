import request from "../../utils/request"
import CheckAuth from '../../utils/auth'

// pages/details/details.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        info:null,
    },

    // 点击图片放大
    showTitle(e){
        wx.previewImage({
            current: e.currentTarget.dataset.current,
            urls:[this.data.info[0].coming.img]
        })
    },

    handleWant(){
        CheckAuth(()=>{
            console.log("准备标记")
            request({
                url:`/watch?comingId=${this.data.info[0].comingId}&tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`
            }).then(res=>{
                // console.log(res)
                if (res.length===0) {
                    request({
                        url:'/watch',
                        method:'post',
                        data:{
                            img:this.data.info[0].coming.img,
                            nm:this.data.info[0].coming.nm,
                            comingTitle:this.data.info[0].coming.comingTitle,
                            star:this.data.info[0].coming.star,
                            wish:this.data.info[0].coming.wish,
                            comingId:this.data.info[0].coming.id,
                            tel:wx.getStorageSync('tel'),
                            nickName:wx.getStorageSync('token').nickName
                        }
                    })
                    wx.showToast({
                      title: '标记成功！',
                    })
                }else{
                    wx.showToast({
                      title: '已标记！',
                    })
                }
            })
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
      wx.setNavigationBarTitle({ //设置导航栏的标题
        title: options.name,
      })
        // console.log(options),
        request({
            url:`/details2?_expand=coming&comingId=${options.id}`
        }).then(res=>{
            console.log(res)
            this.setData({
                info:res
            })
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})