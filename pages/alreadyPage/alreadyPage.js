// pages/alreadyPage/alreadyPage.js
import request from "../../utils/request"
const QR = require('../../utils/wxqrcode');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        movieList:[],
        qrcode:''
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        console.log(options)
        request({
            url:`/buymovies?id=${options.pageid}`
        }).then(res=>{
            console.log(res)
            this.setData({
                movieList:res
            })
        })
        setTimeout(() => {
            this.getQRCodeSize()
            this.createQRCode()
        }, 1000);
        
    },

    getQRCodeSize(){
        var size = 0;
        try{
            var res = wx.getSystemInfoSync(); //异步获取系统信息。需要一定的微信客户端版本支持，在不支持的客户端上，会使用同步实现来返回
            var scale = res.windowWidth / 750//不同屏幕下QRcode的适配比例；设计稿是750rpx宽
            var width = 300 * scale
            size = width
        }catch(e){
            console.log('获取设备信息失败'+e)
            size = 150
        }
        return size
    },

    createQRCode(size){
        try{
            let img = QR.createQrCodeImg(('电影名：')+('《')+(this.data.movieList[0].nm)+('》，')+('总金额为:')+(this.data.movieList[0].price)+('元'),{size:parseInt(size)}) //parseInt解析到整数部分
            this.setData({
                qrcode:img,
            })
        }catch(e){
            console.log(e)
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
        
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})