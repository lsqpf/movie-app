// pages/cinema/cinema.js
import request from "../../utils/request"
import CheckAuth from "../../utils/auth"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        cinemaList:[],
        movieList:[],
        messageLIst:[{
            "bingeWatch": 0,
            "boxInfo": "上映11天，累计票房25323万",
            "cat": "喜剧,剧情",
            "civilPubSt": 0,
            "comingTitle": "9月9日 周五",
            "desc": "主演:马丽,常远,魏翔",
            "dir": "张栾",
            "dur": 111,
            "effectShowNum": 0,
            "followst": 0,
            "globalReleased": true,
            "haspromotionTag": false,
            "headLineShow": false,
            "id": 1335230,
            "img": "https://p0.pipi.cn/mmdb/25bfd6ddc7ef2a3ba350c88741a99730f1b1d.jpg?imageMogr2/thumbnail/2500x2500%3E",
            "isRevival": false,
            "late": false,
            "localPubSt": 0,
            "mark": false,
            "mk": 9.1,
            "movieType": 0,
            "nm": "哥，你好",
            "pn": 71,
            "preSale": 0,
            "preShow": false,
            "proScore": 0,
            "proScoreNum": 0,
            "pubDate": 1662652800000,
            "pubDesc": "2022-09-09 12:00中国大陆上映",
            "pubShowNum": 0,
            "recentShowDate": 1663516800000,
            "recentShowNum": 0,
            "rt": "2022-09-09",
            "sc": 9.1,
            "scm": "马丽魏翔喜剧新片，又笑又催泪",
            "scoreLabel": "猫眼购票评分",
            "showCinemaNum": 293,
            "showInfo": "今天293家影院放映2378场",
            "showNum": 2378,
            "showStateButton": {
              "color": "#F03D37",
              "content": "购票",
              "onlyPreShow": false
            },
            "showTimeInfo": "2022-09-09上映",
            "showst": 3,
            "snum": 65561,
            "star": "马丽,常远,魏翔",
            "totalShowNum": 6890,
            "ver": "IMAX 2D/中国巨幕2D/CINITY 2D/2D",
            "videoId": 489777,
            "videoName": "《哥，你好》今日上映曝终极预告 合家欢乐迎中秋",
            "videourl": "https://vod.pipi.cn/fec9203cvodtransbj1251246104/3ebbc90a387702305762746101/v.f42905.mp4",
            "vnum": 13,
            "vodPlay": false,
            "wish": 436760,
            "wishst": 0
          },],
        TodayTimeList:[],
        TomorrowTimeList:[],
        afterTomorrowList:[],
        current:0,
        timePage:1,
        todayDate:null,
        tomorrowDate:null,
        afterTomorrowDate:null
      },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        // console.log(options)
        this.setData({
          current:options.newCurrent
        })
        wx.setNavigationBarTitle({ //设置导航栏的标题
            title: options.name,
          })
        request({
            url:`/cinemas?id=${options.id}`
        }).then(res=>{
            // console.log(res)
            this.setData({
                cinemaList:res
            })
        })

        request({
          url:`/movies?id=${options.newId}`
        }).then(res=>{
          this.setData({
            messageLIst:res
          })
        })

        this.renderMovies()
        this.renderTodayTime()
        this.renderTomorrow()
        this.renderAfterTomorrow()
        this.renderDate()
    },

    renderMovies(){
        request({
            url:"/movies"
        }).then(res=>{
            // console.log(res)
            this.setData({
                movieList:res
            })
        })
    },

    selectMovies(e){
      var id = e.currentTarget.dataset.id
      request({
        url:`/movies?id=${id}`
      }).then(res=>{
        // console.log(res)
        this.setData({
            messageLIst:res
        })
      })
    },

    renderTodayTime(){
        request({
            url:"/today"
        }).then(res=>{
            // console.log(res)
            this.setData({
                TodayTimeList:res
            })
        })
    },
    renderTomorrow(){
        request({
            url:"/tomorrow"
        }).then(res=>{
            // console.log(res)
            this.setData({
                TomorrowTimeList:res
            })
        })
    },
    renderAfterTomorrow(){
        request({
            url:"/afterTomorrow"
        }).then(res=>{
            // console.log(res)
            this.setData({
                afterTomorrowList:res
            })
        })
    },

    changeToday(){
        this.setData({
            timePage:1
        })
    },

    changeTomorrow(){
        this.setData({
            timePage:2
        })
    },

    changeAfterTomorrow(){
        this.setData({
            timePage:3
        })
    },

    showToday(e){
      var moment = e.currentTarget.dataset.moment  
      var time1 = e.currentTarget.dataset.time1
      var empty1 = e.currentTarget.dataset.empty1  
      var price = e.currentTarget.dataset.price  
      var TodayDate = this.data.todayDate
      var cinema = this.data.cinemaList[0].nm
      if (this.data.messageLIst.length === 0) {
        wx.showToast({
          title: '请选择电影！',
          icon:'none'
        })
      }else{
        wx.navigateTo({ 
            url: `/pages/order/order?moment=${moment}&time1=${time1}&empty1=${empty1}&price=${price}&Date=${TodayDate}&cinema=${cinema}`,
            success:(res)=>{
                // 通过 eventChannel 向被打开页面传送数据
                res.eventChannel.emit("",{data:this.data.messageLIst})
                // console.log(res)
            }
          })
      }
        
    },
    showTomorrow(e){
      var moment = e.currentTarget.dataset.moment  
      var time1 = e.currentTarget.dataset.time1
      var empty1 = e.currentTarget.dataset.empty1  
      var price = e.currentTarget.dataset.price  
      var tomorrowDate = this.data.tomorrowDate
      var cinema = this.data.cinemaList[0].nm
      if (this.data.messageLIst.length === 0) {
        wx.showToast({
            title: '请选择电影！',
            icon:'none'
          })
      }else{
        wx.navigateTo({ 
            url: `/pages/order/order?moment=${moment}&time1=${time1}&empty1=${empty1}&price=${price}&Date=${tomorrowDate}&cinema=${cinema}`,
            success:(res)=>{
                // 通过 eventChannel 向被打开页面传送数据
                res.eventChannel.emit("",{data:this.data.messageLIst})
                // console.log(res)
            }
          })
      }
        
    },
    showAfterTomorrow(e){
      var moment = e.currentTarget.dataset.moment  
      var time1 = e.currentTarget.dataset.time1
      var empty1 = e.currentTarget.dataset.empty1  
      var price = e.currentTarget.dataset.price  
      var afterTomorrowDate = this.data.afterTomorrowDate
      var cinema = this.data.cinemaList[0].nm
      if (this.data.messageLIst.length === 0) {
        wx.showToast({
            title: '请选择电影！',
            icon:'none'
          })
      }else{
        wx.navigateTo({ 
            url: `/pages/order/order?moment=${moment}&time1=${time1}&empty1=${empty1}&price=${price}&Date=${afterTomorrowDate}&cinema=${cinema}`,
            success:(res)=>{
                // 通过 eventChannel 向被打开页面传送数据
                res.eventChannel.emit("",{data:this.data.messageLIst})
                // console.log(res)
            }
          })
      }
    },

    renderDate(){
      var dd = new Date();
      // var y = dd.getFullYear();
      var m = dd.getMonth() + 1;//获取当前月份的日期
      var d = dd.getDate();
      this.setData({
        todayDate:m + "月" + d + "日",
        tomorrowDate:m + "月" + Number(d+1) + "日",
        afterTomorrowDate:m + "月" + Number(d+2) + "日"
      })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})