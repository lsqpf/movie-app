// pages/telephone/telephone.js
import request from "../../utils/request"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tel:""
    },

    formInputChange(e){
        this.setData({
            tel:e.detail.value
        })
    },

    submitForm(){
        wx.setStorageSync('tel', this.data.tel)
        request({
            url:`/users?tel=${this.data.tel}&nickName=${wx.getStorageSync('token').nickName}`
        }).then(res=>{
            // console.log(res)  //如果得到有数据的数组，则是老用户，如果是空数组，则是新用户
            if (res.length===0) {
                request({
                    url:"/users",
                    method:"post",  //平时都是默认get方法，post方法是插入方法
                    data:{
                        ...wx.getStorageSync('token'),
                        tel:this.data.tel
                    }
                }).then(res=>{
                    wx.navigateBack({ //往回退
                      delta: 2,  //退两级
                    })
                    wx.showToast({
                      title: wx.getStorageSync('token').nickName+ ',欢迎你！',
                    })
                })
            }else{
                wx.navigateBack({
                  delta: 2,
                })
                wx.showToast({
                    title: wx.getStorageSync('token').nickName+ ',欢迎你！',
                  })
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})