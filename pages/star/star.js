// pages/star/star.js
import request from "../../utils/request"
import CheckAuth from "../../utils/auth"
Page({

    /**
     * 页面的初始数据
     */
    data:{
        slideButtons:[{
            type:'warn',
            text:'删除',
            extClass:'abb'
        }],
        danmuList: [{
          text: '不错不错',
          color: '#ff0000',
          time: 1
        }, {
          text: '哈哈哈哈',
          color: '#ff00ff',
          time: 3
        },
        {
          text: '果断关注了',
          color: '#ffffff',
          time: 5
        }
      ],
       movieList:[]
      },

      slideButtonTap(e){
        //   console.log(e)
        var pageid = e.currentTarget.dataset.pageid
        console.log(pageid)
        this.setData({
            movieList:this.data.movieList.filter(item=>item.id !== pageid) //过滤出item.id不等于传入的id的商品
        })
        request({
            url:`/star/${pageid}`,
            method:"delete"
        })
      },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
            let {nickName} = wx.getStorageSync('token')
            let tel = wx.getStorageSync('tel')
            request({
                url:`/star?nickName=${nickName}&tel=${tel}`
            }).then(res=>{
                // console.log(res)
                this.setData({
                    movieList:res
                })
            })        
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})