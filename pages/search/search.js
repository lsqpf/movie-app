// pages/search/search.js
import request from "../../utils/request"

Page({

    /**
     * 页面的初始数据
     */
    data: {

    },

    search(value){
        return Promise.all([
            request({
                url:`/movies?nm_like=${value}`
            }),
            request({
                url:`/comings?nm_like=${value}`
            })
        ]).then(res=>{
            return [...res[0].map(item=>({
                ...item,
                text:item.nm,
                type:1
            })),...res[1].map(item=>({
                ...item,
                text:item.nm,
                type:2
            }))]
        })
    },

    selectResult(e){
        // console.log(e)
        if (e.detail.item.type===1) {
            wx.navigateTo({
              url: `/pages/details/details?id=${e.detail.item.id}&name=${e.detail.item.nm}`,
            })
        }else{
            wx.navigateTo({
              url: `/pages/trailer/trailer?id=${e.detail.item.id}&name=${e.detail.item.nm}`
            })
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.setData({
            search:this.search.bind(this)  // 避免找不到search
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})