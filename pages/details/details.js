import request from "../../utils/request"
import CheckAuth from '../../utils/auth'

// pages/details/details.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        info:null,
        commentsList:[],
        content:"",
        CurrentData:''
    },

    // 点击图片放大
    showTitle(e){
        wx.previewImage({
            current: e.currentTarget.dataset.current,
            urls:[this.data.info[0].movie.img]
        })
    },

    handlelike(){
        CheckAuth(()=>{
            console.log("准备收藏")
            request({
                url:`/like?movieId=${this.data.info[0].movieId}&tel=${wx.getStorageSync('tel')}&nickName=${wx.getStorageSync('token').nickName}`
            }).then(res=>{
                // console.log(res)
                if (res.length===0) {
                    request({
                        url:"/like",
                        method:"post",
                        data:{
                            movieId:this.data.info[0].movieId,
                            img:this.data.info[0].movie.img,
                            nm:this.data.info[0].movie.nm,
                            cat:this.data.info[0].movie.cat,
                            desc:this.data.info[0].movie.desc,
                            dur:this.data.info[0].movie.dur,
                            tel:wx.getStorageSync('tel'),
                            nickName:wx.getStorageSync('token').nickName
                        }
                    })
                    wx.showToast({
                      title: '收藏成功！',
                    })
                }else{
                    wx.showToast({
                      title: '已收藏！',
                    })
                }
            })
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
    //   console.log(options)
      wx.setNavigationBarTitle({ //设置导航栏的标题
        title: options.name,
      })
        // console.log(options),
        request({
            url:`/details?_expand=movie&movieId=${options.id}`
        }).then(res=>{
            // console.log(res)
            this.setData({
                info:res
            })
        })
        this.renderComments()
    },

    renderComments(){
      request({
        url:"/comments"
      }).then(res=>{
        // console.log(res)
        this.setData({
          commentsList:res
        })
      })
    },

    BuyPage(){
    //   console.log(this.data.info)
      var pageItem = 3
      wx.reLaunch({
        url: `/pages/home/home?pageItem=${pageItem}&info=${this.data.info[0].movie.id}`,
      })
    },

    handleSubmit(){
        CheckAuth(()=>{
            console.log("准备评论")
            var dd = new Date();
            var y = dd.getFullYear();
            var m = dd.getMonth() + 1;
            var d = dd.getDate();
            var h = dd.getHours();
            var n = dd.getMinutes();
            var s = dd.getSeconds();
            this.setData({
                CurrentData:y+'-'+m+'-'+d+' '+h+':'+n+':'+s
            })
            if (this.data.content.length===0) {
                wx.showToast({
                  title: '请输入您的影评！',
                  icon:'none'
                })
            }else{
                request({
                    url:'/comments',
                    method:'post',
                    data:{
                        content:this.data.content,
                        creationTime:this.data.CurrentData,
                        avatarUrl:wx.getStorageSync('token').avatarUrl,
                        tel:wx.getStorageSync('tel'),
                        nickName:wx.getStorageSync('token').nickName
                    }
                })
                wx.showToast({
                  title: '评论成功',
                })
                this.renderComments()
            }
        })
        this.setData({
          content:''
        })
        
    },

    inputPage(e){
        console.log(e.detail.value)
        this.setData({
            content:e.detail.value
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})