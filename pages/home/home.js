// pages/home/home.js
import request from "../../utils/request"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        movieList: [],
        slideList: [],
        CurrentCity: '南宁',
        no_scroll: true,
        pageItem: 1,
        comingList: [],
        cinemaList: [],
        select: false,
        tihuoWay: '全城',
        selectprice: '筛选',
        handleprice: false,
        newCurrent:null,
        newId:null
    },

    current: 1,
    total: 0,
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
    //   console.log(options)
      this.setData({
        newId:options.info
      })
      if (options.pageItem === undefined) {
        this.setData({
          pageItem:1
        })
      }else{
        this.setData({
        pageItem:Number(options.pageItem)
      })
      }
        request({
            url: `/cities?cityid=${options.id}`
        }).then(res => {
            // console.log(res[0].name)
            this.setData({
                CurrentCity: res[0].name
            })
        })

        request({
          url:`/movies?id=${options.info}`
        }).then(res=>{
        //   console.log(res[0].nm)
          wx.setNavigationBarTitle({
            title: res[0].nm,
          })
          this.setData({
            newCurrent:res[0].current
          })
        })

        this.renderMovies(),
            this.renderSlide(),
            this.randerComing(),
            this.renderCinema()
    },

    renderMovies() {
        request({
            url: `/movies?_page=${this.current}&_limit=4` //表示在goods中取第1页限制4条的数据
        }, true).then(res => {
            // console.log(res)
            this.total = Number(res.total) //转化为number类型
            this.setData({
                movieList: [...this.data.movieList, ...res.list]
            })
        })
    },

    renderSlide() {
        request({
            url: "/recommends"
        }).then(res => {
            // console.log(res)
            this.setData({
                slideList: res
            })
        })
    },

    chooseCity() {
        wx.navigateTo({
            url: '../cities/cities',
        })
    },

    // 只要用户在 Page 构造时传入了 onPageScroll 监听，基础库就会认为开发者需要监听页面 scoll 事件
    onPageScroll(e) {
        if (e.scrollTop > 200) {
            this.setData({
                no_scroll: false
            });
        } else {
            this.setData({
                no_scroll: true
            });
        }
    },

    goTop() {
        if (wx.pageScrollTo) { //当里面的值为真时，点击则回到顶部，若为假，因为上面的代码，也就不会显示出来
            wx.pageScrollTo({
                scrollTop: 0 //scrollTop: 滚动到页面的目标位置
            })
        }
    },

    // 点击切换函数
    showHot() {
        this.setData({
            pageItem: 1
        })
    },
    showNew() {
        this.setData({
            pageItem: 2
        })
    },
    showCinema() {
        this.setData({
            pageItem: 3
        })
    },

    randerComing() {
        request({
            url: "/comings"
        }).then(res => {
            this.setData({
                comingList: res
            })
        })
    },

    renderCinema() {
        request({
            url: "/cinemas"
        }).then(res => {
            this.setData({
                cinemaList: res
            })
        })
    },

    // 筛选部分函数
    bindShowMsg() {
        this.setData({
            select: !this.data.select
        })
    },
    mySelect(e) {
        var name = e.currentTarget.dataset.name
        this.setData({
            tihuoWay: name,
            select: false
        })
        request({
            url: `/cinemas?addr_like=${name}`
        }).then(res => {
            if (res.length === 0) {
                this.renderCinema()
            } else {
                this.setData({
                    cinemaList: res
                })
            }
        })
        console.log(name)
    },
    bindShowPrice() {
        this.setData({
            handleprice: !this.data.handleprice
        })
    },
    Selectprice(e) {
        var price = e.currentTarget.dataset.price
        this.setData({
            selectprice: price,
            handleprice: false,
            cinemaList: price === "价格升序" ? this.data.cinemaList.sort((item1, item2) => item1.sellPrice - item2.sellPrice) : this.data.cinemaList.sort((item1, item2) => item2.sellPrice - item1.sellPrice)
        })
    },

    handleSearch() {
        wx.navigateTo({
            url: '/pages/search/search',
        })
    },

    changePage(e) {
        var id = e.currentTarget.dataset.id
        var name = e.currentTarget.dataset.name
        wx.navigateTo({
            url: `/pages/details/details?id=${id}&name=${name}`,
        })
    },

    changeNewPage(e) {
        var id = e.currentTarget.dataset.id
        var name = e.currentTarget.dataset.name
        wx.navigateTo({
            url: `/pages/trailer/trailer?id=${id}&name=${name}`,
        })
    },

    tap(e) {
        var id = e.currentTarget.dataset.id
        var name = e.currentTarget.dataset.name
        wx.navigateTo({
            url: `/pages/details/details?id=${id}&name=${name}`,
        })
    },

    openCinemas(e){
        var id = e.currentTarget.dataset.id
        var name = e.currentTarget.dataset.name
        wx.navigateTo({
          url: `/pages/cinema/cinema?id=${id}&name=${name}&newCurrent=${this.data.newCurrent}&newId=${this.data.newId}`,
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {},

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        console.log("下拉了")
        setTimeout(() => {
            // 更新数据
            wx.reLaunch({
              url: '/pages/home/home',
            })

            wx.stopPullDownRefresh({ //停止下拉刷新
                success: (res) => {

                },
            })
        }, 1000)
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {
        if (this.data.movieList.length === this.total) {
            return
        }
        console.log("到底了")
        this.current++
        this.renderMovies()
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})