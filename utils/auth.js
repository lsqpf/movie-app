function CheckAuth(callback) {
    if (wx.getStorageSync('tel')) {
        callback()
    }else{
        if (wx.getStorageSync('token')) {
            wx.navigateTo({
              url: '/pages/telephone/telephone',
            })
        }else{
            wx.navigateTo({
              url: '/pages/auth/auth',
            })
        }
    }
}

export default CheckAuth