# MovieApp

#### 介绍
使用微信开发者工具制作的关于电影订购的微信小程序；主要技术包括：getUserProfile获取授权用户信息、onReachBottom实现懒加载效果、WeUI组件库、wxqrcode.js支持二维码的生成、json-server实现前后端分离等。实现存储用户信息和用户操作，对用户操作信息进行修改与查看等功能。

#### 安装
 _json-server，微信开发者工具_ 


#### 使用说明

本项目使用json-server存储数据，一个在前端本地运行，可以存储json数据的server；通俗来说，就是模拟服务端接口数据。一般用在前后端分离后，前端人员可以不依赖API开发，而在本地搭建一个JSON服务，自己产生测试数据。

第一步：打开命令提示符，使用npm命令：npm install -g json-server全局安装json-server
第二步：在文件夹myserver中打开命令提示符使用：json-server --watch db.json命令打开server
第三步：打开微信开发者工具，导入MovieApp文件

注意：myserver文件夹不与其他文件共同放置，myserver文件夹打开后cmd启动json-server,
没有json-server的自行前往下载（需电脑已安装好node），微信开发者工具导入文件

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 程序截图

 _首页-正在热映部分：_ 

![输入图片说明](picture/11111.png)

==============================================

 _首页-附近影院部分：_ 

![输入图片说明](picture/12.png)

==============================================

 _首页-新片预告部分：_ 

![输入图片说明](picture/13.png)

==============================================

 _视频部分：_ 

![输入图片说明](picture/24.png)

==============================================

 _详情页部分：_ 

![输入图片说明](picture/223.png)

==============================================

 _购票部分：_ 

![输入图片说明](picture/111.png)

==============================================

 _个人页面(未登录)：_ 

![输入图片说明](picture/1c.png)

==============================================

 _个人页面（登录）：_ 

![输入图片说明](picture/2e.png)

==============================================

 **以上页面仅为主要页面，不代表所有页面展示** 