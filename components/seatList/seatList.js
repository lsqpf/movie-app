// components/seatList/seatList.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
      seatings: Array,
    },
  
    /**
     * 组件的初始数据
     */
    data: {
      selectedIndex: [],
      selectedNum: 0,
      seatList:[],
      newSeatList:[]
    },
    /**
     * 组件的方法列表
     */
    methods: {
      selected(e) {
        let index = e.currentTarget.dataset.index;
        if(this.data.selectedIndex.indexOf(index)!=-1){
          let selectedIndex =  this.remove(this.data.selectedIndex, index);
          let selectedNum = this.data.selectedNum - 1;
          let seatList = this.data.seatList.filter((item)=>item!==index)
          this.setData({
              selectedIndex,
              selectedNum,
              seatList
            })
          console.log(index)
        }else{
          if(this.data.selectedNum < 6){
          let selectedNum = this.data.selectedNum + 1;
          let selectedIndex = this.data.selectedIndex.concat(index);
              this.setData({
                selectedIndex,
                selectedNum,
                seatList:[...this.data.seatList,index]
              })
        }else{
          wx.showToast({
            icon:'none',
            title: '最多选择六张票',
          })
         }
        }
  
        this.setData({
          newSeatList:this.data.seatList.map((item)=>{
              if (item<=13) {
                  return "一排"+item+"座"
              }
              if (item>13 && item<=28) {
                  return "二排"+(item-2-13)+"座"
              }
              if (item>28 && item<=43) {
                  return "三排"+(item-2-28)+"座"
              }
              if (item>43 && item<=58) {
                  return "四排"+(item-2-43)+"座"
              }
              if (item>58 && item<=71) {
                  return "五排"+(item-4-58)+"座"
              }
              if (item>71 && item<=86) {
                  return "六排"+(item-6-71)+"座"
              }
              if (item>86 && item<=101) {
                  return "七排"+(item-6-86)+"座"
              }
          })
      })
        
      this.triggerEvent('ParentEvent',this.data.selectedIndex.length)
      this.triggerEvent('Event',this.data.newSeatList)

      },
      remove(arr, ele) {
        var index = arr.indexOf(ele); 
        if (index > -1) { 
        arr.splice(index, 1); 
          }
          return arr;
        }
      }
  })
  
